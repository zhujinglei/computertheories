## Install an Ubuntu Linux virtual machine on your Macbook:

* You'll need about 10GB free disk space for this to work.

## Step One

* Download and install Virtualbox from https://www.virtualbox.org/wiki/Downloads (OS X Hosts)
_You can run `virtualbox` from the command line to ensure it is installed (doing so will open virtualbox in a separate window)_

_Some of you may encounter an error message stating that Virtualbox has failed to install. If you encounter this error, you will need to take the following steps:_

1. _Eject the Virtualbox from Finder > Devices_
2. _Now allow the exception in: System Preferences > Security & Privacy_
3. _Finally but not least click Allow button so that way the developer with the name "Oracle America, Inc" will be accepted and the installer using that certificate will run just fine._
4. _Then try to install from the .dmg again so that it remounts the device._

_For a visual representation of these steps, take a look at the response to [this](https://apple.stackexchange.com/questions/301303/virtualbox-5-1-28-fails-to-install-on-macos-10-13-due-to-kext-security) question._

## Step Two

* Download and install Vagrant from https://www.vagrantup.com/downloads.html (MacOS 64-bit)
_You can do a quick check that Vagrant has been installed by typing `vagrant --version` in the command line. If it is installed, you will see your Vagrant version printed in the terminal_


## Step Three

* Make a directory to build your virtual machine in, and a subdirectory to be shared between the vm and your mac. 

Open the Terminal app (In Applications/Utilities).  

Then:
```bash
$ mkdir ~/ubuntu
$ cd ~/ubuntu
$ mkdir shared

```

## Step Four

* You will then need to use Vagrant to build and start your Virtual Machine using an existing image (You can copy the appropriate command by going to [this link](https://app.vagrantup.com/philipwilson/boxes/ubuntu-18.04-server) and copying the contents from the "new" tab and pasting it into the command line): 

```bash
$ vagrant init philipwilson/ubuntu-18.04-server \
  --box-version 0.0.1
$ vagrant up
```

## Step Five

* Visit https://gitlab.com/snippets/1754693 and copy the contents into your Vagrantfile (which will have been created in the ubuntu directory when you ran the previous commands). You should delete any existing content.  

## Step Six

* Now you will need to make sure these configuration changes are picked up.  Again in a terminal window:
```bash
$ cd ~/ubuntu
$ vagrant reload 
```

## Step Seven

* If everything went well, you can now log into your new virtual machine:
```bash
$ vagrant ssh
```
This should give you a shell prompt on your vm:
```bash
vagrant@ubuntu:~/
```
On my machine it's green :-)

## Summary

* Congratulations, you're now logged in to your Ubuntu virtual machine.
   * You are logged in as the `vagrant` user with `sudo` access, so you can do whatever you like.
   * You should have a subdirectory called `shared` in the vagrant home directory (check now by typing `ls` from the vagrant home directoru).  This is a mount of `~/ubuntu/shared` on your mac, so files you create in one place should magically appear in the other. 
   * Python3 and Git are already installed.  Knock yourself out!

* When you're done, you can type `exit` to close your session on the vm.  Back at the MacOS shell prompt, you can type:
```bash
$ vagrant halt
```
to stop the virtual machine from running in the background.  

* Whenever you like, you can restart the vm and log in like this:
```bash
$ cd ~/ubuntu
$ vagrant up
$ vagrant ssh
```






